import { Dimensions } from 'react-native';
const React = require("react-native");

const { StyleSheet } = React;
const win = Dimensions.get('window');
export default {

    containerView: {
        flex: 1
    },
    fullheight:{
        flex:1
    },
    imageWidthHeightView:{
        textAlign:"center", 
        paddingLeft:40,
        paddingRight:40 
    },
    imageWidthHeight:{
        width:"100%",
        height:70,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 80,
        marginBottom: 20,
    },
    imageWidthHeight2:{
        width:"100%",
        height:70,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 40,
        marginBottom: 20,
    },
    loginScreenContainer: {
        flex: 1,
    },
    loginFormView: {
        flex: 1,
        textAlign:"Center"
    },
    loginForm:{
        marginTop:100
    },
    landingFormTextInput:{
        fontSize: 16,
        borderBottomColor: 'white',
        borderBottomWidth: 1.5,
        color:"black",
        paddingLeft: 10,
        marginLeft: 40,
        marginRight: 40,
        marginTop: 0,
        marginBottom: 15,
        paddingVertical:10,
    },
    loginFormTextInput: {
        fontSize: 16,
        borderBottomColor: 'white',
        borderBottomWidth: 1.5,
        color:"black",
        paddingLeft: 10,
        textAlign:"center",
        marginLeft: 40,
        marginRight: 40,
        marginTop: 0,
        marginBottom: 15,
    },
    appButtonContainer: {
        marginRight:30,
        marginLeft:30,
        marginTop:20,
        marginBottom:10,
        elevation: 8,
        borderRadius: 30,
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    appButtonText: {
        fontSize: 18,
        color: "#000",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    forgotPasswordButtonText: {
        fontSize: 12,
        color: "#000",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    dateONLAND:{
        textAlign:"right",
        fontWeight: "bold",
    },
    dateOnLandWhite:{
        color:"#fff"
    },
    paddingForLayout:{
        paddingLeft:40,
        paddingRight:40 
    },
    selectLabel:{
        paddingVertical:10,
        fontSize: 14,
        color: "#000",
        fontWeight: '600',
    },
    wareHouse:{
        flex: 1,
        alignSelf: 'stretch',
        aspectRatio: 1.4,
        borderRadius:10,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 0,
        marginBottom: 0,
    },
    halfWidth:{
        margin:2,
        flex: 1,
        borderRadius:15,
        borderWidth:1,
        borderColor:"#fff",
        flexDirection: 'row',
        flexWrap: "wrap"
    },
    QRImageHeight:{
        width:"100%",
        height:70,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 40,
        marginBottom: 20,
    },
    wareHouseName:{
        textAlign:"center",
        color:"#fff",
        fontSize:24,
        marginBottom:15,
        textTransform:"uppercase",
        fontWeight:"bold",
    },
    QRSction:{
        width:"100%",
        marginTop:25,
    },
    qRImage:{
        flex: 1,
        marginTop:0,
       // aspectRatio:0.45,
        width:win.width,
        height:win.width*1.33
        //resizeMode: 'contain',
    },
    mainButtons:{
        width:win.width/100 * 46,
        backgroundColor:"white",
        borderRadius:5,
        marginLeft:win.width/100*4,
    },
    mainButtonsCat:{
        width:win.width/100 * 46,
        backgroundColor:"white",
        borderRadius:5,
        marginRight:win.width/100*4,
    },
    QRSCANQrButton:{
        width:win.width/100 * 50,
        height: 48,
    },
    QRSCANSearchButton:{
        width:win.width/100 * 50,
        height: 48,
    },
    halfWidthButtons:{
        flex: 1,
        borderRadius:18,
        flexDirection: 'row',
        flexWrap: "wrap"
    },
    scene: {
        flex: 1,
        width:win.width,
        height:win.height/100 *80,
        alignSelf: 'stretch',
    },
    tabView:{
        marginTop:win.height/100 *2,
        backgroundColor:"white",
    },
    halfInnerFlatWidth:{
        margin:2,
        flex: 1,
        borderRadius:15,
        borderWidth:1,
        borderColor:"#fff",
        flexDirection: 'row',
        flexWrap: "wrap"
    },
    recentlyImage:{
        width:win.width/100 * 30,
        height:54,
        marginRight:5,
        borderRadius:10,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 0,
        marginBottom: 0,
    },
    Category:{
        flex: 1,
        alignSelf: 'stretch',
        aspectRatio: 1,
        borderRadius:10,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 0,
        marginBottom: 0,
    },
    noLabel: {
        display: 'none',
        height: 0
    },
    bubble: {
        backgroundColor: 'lime',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 10
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    signOutBtn:{
        textAlign:"right",
        color:"white",
        margin:15,
        fontWeight:"bold",
    }
};