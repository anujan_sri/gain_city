// React Native Camera
// https://aboutreact.com/react-native-camera/

// import React in our code
import React, {useState} from 'react';

// import all the components we are going to use
import {
  SafeAreaView,
  Text,
  View,
  PermissionsAndroid,
  Alert,
  Platform,
  TouchableHighlight,
  ScrollView,
  TouchableWithoutFeedback, 
  KeyboardAvoidingView, 
  Keyboard,
  Image,
  TouchableOpacity,
} from 'react-native';

// import CameraKitCameraScreen
import {CameraKitCameraScreen} from 'react-native-camera-kit';
import styles from "../style/Style";

import LinearGradient from 'react-native-linear-gradient'

const App = ({ navigation, route  }) => {
  const [isPermitted, setIsPermitted] = useState(false);
  const [captureImages, setCaptureImages] = useState([]);

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Camera Permission',
          message: 'App needs camera permission',
        },
      );
      // If CAMERA Permission is granted
      return granted === PermissionsAndroid.RESULTS.GRANTED;
    } catch (err) {
      console.warn(err);
      return false;
    }
  };

  const requestExternalWritePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'External Storage Write Permission',
          message: 'App needs write permission',
        },
      );
      // If WRITE_EXTERNAL_STORAGE Permission is granted
      return granted === PermissionsAndroid.RESULTS.GRANTED;
    } catch (err) {
      console.warn(err);
      alert('Write permission err', err);
    }
    return false;
  };

  const requestExternalReadPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Read Storage Permission',
          message: 'App needs Read Storage Permission',
        },
      );
      // If READ_EXTERNAL_STORAGE Permission is granted
      return granted === PermissionsAndroid.RESULTS.GRANTED;
    } catch (err) {
      console.warn(err);
      alert('Read permission err', err);
    }
    return false;
  };

  const openCamera = async () => {
    if (Platform.OS === 'android') {
      if (await requestCameraPermission()) {
        if (await requestExternalWritePermission()) {
          if (await requestExternalReadPermission()) {
            setIsPermitted(true);
          } else alert('READ_EXTERNAL_STORAGE permission denied');
        } else alert('WRITE_EXTERNAL_STORAGE permission denied');
      } else alert('CAMERA permission denied');
    } else {
      setIsPermitted(true);
    }
  };

  const onBottomButtonPressed = (event) => {
    const images = JSON.stringify(event.captureImages);
    if (event.type === 'left') {
      setIsPermitted(false);
    } else if (event.type === 'right') {
      setIsPermitted(false);
      setCaptureImages(images);
    } else {
      Alert.alert(
        event.type,
        images,
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {isPermitted ? (
        <View style={{flex: 1}}>
          <CameraKitCameraScreen
            // Buttons to perform action done and cancel
            actions={{
              rightButtonText: 'Done',
              leftButtonText: 'Cancel'
            }}
            onBottomButtonPressed={
              (event) => onBottomButtonPressed(event)
            }
            flashImages={{
              // Flash button images
              on: require('../assets/flashon.png'),
              off: require('../assets/flashoff.png'),
              auto: require('../assets/flashauto.png'),
            }}
            cameraFlipImage={require('../assets/flip.png')}
            captureButtonImage={require('../assets/capture.png')}
          />
        </View>
      ) : (
        <View style={styles.container}>
            <LinearGradient
            colors={[ '#f7b614','#f7b614', 'white' ]}
            style={styles.fullheight}
            >
                <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>

                    <KeyboardAvoidingView style={styles.containerView} behavior="padding">

                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <View style={styles.loginScreenContainer}>
                                <View style={styles.loginFormView}> 
                                    <View style={styles.imageWidthHeightView}> 
                                        <Image 
                                        source = {require("../assets/Logo.png")} 
                                        style={styles.QRImageHeight}
                                        />
                                        <Text style={styles.wareHouseName}>{route.params.name} Warehouse</Text>
                                    </View>  
                                    <View
                                        style={styles.halfWidthButtons}
                                        >
                                        <TouchableOpacity 
                                            onPress={() =>navigation.navigate('QRScreen', { name: route.params.name })} 
                                            style={styles.mainButtons}
                                        >
                                            <Image 
                                            source = {require('../assets/scan-qr.png')} 
                                            style={styles.QRSCANQrButton}
                                            resizeMode={'contain'} 
                                            />
                                        </TouchableOpacity>
                                        <TouchableOpacity 
                                            onPress={() =>navigation.navigate('SelectItemCatButton', { name: route.params.name })} 
                                        >
                                            <Image 
                                            source = {require('../assets/search.png')} 
                                            style={styles.QRSCANSearchButton}
                                            resizeMode={'contain'} 
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.QRSction}> 
                                        <TouchableOpacity 
                                            onPress={openCamera} 
                                        >
                                            <Image 
                                            source = {require("../assets/qr-scan.png")} 
                                            style={styles.qRImage}
                                            />
                                        </TouchableOpacity>  
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </KeyboardAvoidingView>
                </ScrollView>
            </LinearGradient>
        </View>
      )}
    </SafeAreaView>
  );
};

export default App;

