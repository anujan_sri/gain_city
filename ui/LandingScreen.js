import React, { useState, useEffect ,  Component }  from 'react';
import {
  ScrollView,
  View,
  TouchableWithoutFeedback, 
  KeyboardAvoidingView, 
  Keyboard,
  Image,
  Text,
  FlatList,
  TouchableOpacity, 
} from 'react-native';

import styles from "../style/Style";

import auth from '@react-native-firebase/auth';

import LinearGradient from 'react-native-linear-gradient'

const DATA = [
    {
      id: "1",
      title: "SK",
      image:require('../assets/sk-wh.png'),
    },
    {
      id: "2",
      title: "AMK",
      image:require('../assets/amk-wh.png'),
    },
  ];

// export default LandingScreen;

export default class LandingScreen extends Component {
    constructor() {
      super();
      this.state = { 
        uid: ''
      }
    }
  
    signOut = () => {
      auth().signOut().then(() => {
        this.props.navigation.navigate('LoginScreen')
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }  
  
    render() {
    //   this.state = { 
    //     displayName: firebase.auth().currentUser.displayName,
    //     uid: firebase.auth().currentUser.uid
    //   }    
      return (
        <LinearGradient
            colors={[ '#f7b614','#f7b614', 'white' ]}
            style={styles.fullheight}
            >
            <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>

                <KeyboardAvoidingView style={styles.containerView} behavior="padding">

                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.loginScreenContainer}>
                            <View style={styles.loginFormView}> 
                                <View>
                                    <TouchableOpacity 
                                        onPress={() => this.signOut()}
                                    >
                                        <Text 
                                        style={styles.signOutBtn}>Logout</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.imageWidthHeightView}> 
                                    <Image 
                                    source = {require("../assets/Logo.png")} 
                                    style={styles.imageWidthHeight2}
                                    />
                                    <Text style={styles.dateONLAND}>Date: <Text style={styles.dateOnLandWhite}>12/15/2020</Text></Text>
                                </View>  
                                <View style={styles.loginForm}>
                                    <Text style={styles.landingFormTextInput} >Staff Code: 11223344</Text>
                                    <Text style={styles.landingFormTextInput} >Name: Anujan</Text>
                                    <View style={styles.paddingForLayout}>
                                        <Text style={styles.selectLabel}>Select Warehouse:</Text>
                                        <FlatList
                                            data={DATA}
                                            renderItem={({item}) => (
                                            <View
                                                style={{
                                                flex: 1,
                                                flexDirection: 'column',
                                                margin: 1
                                                }}>
                                                <TouchableOpacity 
                                                    onPress={()=> this.props.navigation.navigate('QRScreen', { name: item.title })} 
                                                // onPress={() =>navigation.navigate('camera', { name: item.title })} 
                                                    style={styles.halfWidth}
                                                >
                                                    <Image 
                                                    source = {item.image} 
                                                    style={styles.wareHouse}
                                                    resizeMode={'contain'} 
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                            )}
                                            //Setting the number of column
                                            numColumns={2}
                                            keyExtractor={(item, index) => index}
                                        />
                                    </View>
                                </View>

                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>

            </ScrollView>
        </LinearGradient>
      );
    }
  }
