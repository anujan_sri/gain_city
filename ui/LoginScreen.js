/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component }  from 'react';
import {
  ScrollView,
  View,
  TextInput, 
  TouchableWithoutFeedback, 
  KeyboardAvoidingView, 
  Keyboard,
  Image,
  Alert,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import auth from '@react-native-firebase/auth';

import styles from "../style/Style";

import LinearGradient from 'react-native-linear-gradient'


export default class LoginScreen extends Component {
  
  constructor() {
    super();
    this.state = { 
      email: '', 
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert(
        'Sorry!',
        'Enter details to signin!',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          },
          { text: 'OK', onPress: () => console.log('OK Pressed') }
        ],
        { cancelable: false }
      );
    } else {
      this.setState({
        isLoading: true,
      })
      auth().signInWithEmailAndPassword(this.state.email,this.state.password)
        ///.createUserWithEmailAndPassword("anujancis@gmail.com", "123456")
        .then(() => {
          console.log('Successfully Login');
          this.setState({
            isLoading: false,
            email: '', 
            password: ''
          })
          this.props.navigation.navigate('LandingScreen')
        })
        .catch(error => {
          this.setState({
            isLoading: false, 
            email: '', 
            password: ''
          })
          if (error.code == 'auth/user-not-found') {
            Alert.alert(
              'User not found',
              'That email address is not register!',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                { text: 'OK', onPress: () => console.log('OK Pressed') }
              ],
              { cancelable: false }
            );
            console.log('That email address is already in use!');
          }
          else if (error.code  == 'auth/invalid-email') {
            Alert.alert(
              'Invalid email',
              'That email address is invalid!',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                { text: 'OK', onPress: () => console.log('OK Pressed') }
              ],
              { cancelable: false }
            );
            //console.log('That email address is invalid!');
          }
          Alert.alert(
            'Login Error',
            error.code,
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') }
            ],
            { cancelable: false }
          );
          //console.error(error.code);
        });
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      <LinearGradient
      colors={[ '#f7b614','#f7b614', 'white' ]}
      style={styles.fullheight}
      >
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>

            <KeyboardAvoidingView style={styles.containerView} behavior="padding">

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.loginScreenContainer}>
                        <View style={styles.loginFormView}> 
                            <View style={styles.imageWidthHeightView}> 
                                <Image 
                                source = {require("../assets/Logo.png")} 
                                style={styles.imageWidthHeight}
                                />
                            </View>  
                            <View style={styles.loginForm}>
                                <TextInput 
                                placeholder="Email"
                                value={this.state.email}
                                onChangeText={(val) => this.updateInputVal(val, 'email')}
                                style={styles.loginFormTextInput} 
                                />
                                <TextInput
                                placeholder="Password"
                                value={this.state.password}
                                onChangeText={(val) => this.updateInputVal(val, 'password')}
                                maxLength={15}
                                secureTextEntry={true} 
                                style={styles.loginFormTextInput} 
                                />
                                <View>
                                  <TouchableOpacity 
                                   // onPress={() =>navigation.navigate('LandingScreen')}
                                   onPress={() => this.userLogin()}
                                    //onPress={LoginFunction}
                                  >
                                    <LinearGradient
                                      colors={["#d0cfcf", "#ffffff"]}
                                      style={styles.appButtonContainer}
                                    >
                                      <Text style={styles.appButtonText}>Login</Text>
                                    </LinearGradient>
                                  </TouchableOpacity>
                                  
                                  <TouchableOpacity onPress={() =>navigation.navigate('ForgotPassword')}>
                                      <Text style={styles.forgotPasswordButtonText}>Forgot Password</Text>
                                  </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>

        </ScrollView>
    </LinearGradient>
    );
  }
}