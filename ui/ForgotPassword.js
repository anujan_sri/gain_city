import React from 'react';
import {
  ScrollView,
  View,
  TextInput, 
  TouchableWithoutFeedback, 
  KeyboardAvoidingView, 
  Keyboard,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';


import styles from "../style/Style";

import LinearGradient from 'react-native-linear-gradient'

const ForgotPassword = ({ navigation }) => {
  return (
      <LinearGradient
            colors={[ '#f7b614','#f7b614', 'white' ]}
            style={styles.fullheight}
            >
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>

            <KeyboardAvoidingView style={styles.containerView} behavior="padding">

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.loginScreenContainer}>
                        <View style={styles.loginFormView}> 
                            <View style={styles.imageWidthHeightView}> 
                                <Image 
                                source = {require("../assets/Logo.png")} 
                                style={styles.imageWidthHeight}
                                />
                            </View>  
                            
                            <View style={styles.loginForm}>
                                <TextInput placeholder="Current Password"  style={styles.loginFormTextInput} />
                                <TextInput placeholder="New Password"  style={styles.loginFormTextInput} secureTextEntry={true}/>
                                <View>
                                  <TouchableOpacity onPress={() =>navigation.navigate('LoginScreen')}>
                                    <LinearGradient
                                      colors={["#d0cfcf", "#ffffff"]}
                                      style={styles.appButtonContainer}
                                    >
                                      <Text style={styles.appButtonText}>UPDATE</Text>
                                    </LinearGradient>
                                  </TouchableOpacity>
                                </View>
                            </View>
                            
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>

        </ScrollView>
    </LinearGradient>
  );
  
};


export default ForgotPassword;

