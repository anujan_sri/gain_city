import React from 'react';
import {
  ScrollView,
  View, 
  TouchableWithoutFeedback, 
  KeyboardAvoidingView, 
  Keyboard,
  Image,
  Text,
  TouchableOpacity, 
  FlatList, 
  Dimensions,
} from 'react-native';

import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
import styles from "../style/Style";
import LinearGradient from 'react-native-linear-gradient'

const DATA1 = [
    {
        id: "1",
        image:require('../assets/accessories.png'),
    },
    {
        id: "2",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "3",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "4",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "5",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "6",
        image:require('../assets/accessories.png'),
    },
    {
        id: "7",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "8",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "9",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "10",
        image:require('../assets/accessories.png'),
    },
    {
        id: "11",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "12",
        image:require('../assets/bolt-nut.png'),
    },
  ];
  const DATA2 = [
    {
        id: "1",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "2",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "3",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "4",
        image:require('../assets/accessories.png'),
    },
    {
        id: "5",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "6",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "7",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "8",
        image:require('../assets/accessories.png'),
    },
    {
        id: "9",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "10",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "11",
        image:require('../assets/accessories.png'),
    },
    {
        id: "12",
        image:require('../assets/bolt-nut.png'),
    },
  ];
  const DATA3 = [
    {
        id: "1",
        image:require('../assets/accessories.png'),
    },
    {
        id: "2",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "3",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "4",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "5",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "6",
        image:require('../assets/accessories.png'),
    },
    {
        id: "7",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "8",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "9",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "10",
        image:require('../assets/accessories.png'),
    },
    {
        id: "11",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "12",
        image:require('../assets/bolt-nut.png'),
    },
  ];

export default function QRScreen({ navigation, route  }) {

    const FirstRoute = () => (
        <View style={[styles.scene, { backgroundColor: '#ffffff',paddingTop:20 }]}>
            <View>
                <Text style={{margin:10}}>CATEGORY</Text>
                <FlatList
                    data={DATA1}
                    renderItem={({item}) => (
                    <View
                        style={{
                        flex: 1,
                        flexDirection: 'column',
                        margin: 1
                        }}>
                        <TouchableOpacity 
                            style={styles.halfWidth}
                        >
                            <Image 
                            source = {item.image} 
                            style={styles.Category}
                            resizeMode={'contain'} 
                            />
                        </TouchableOpacity>
                    </View>
                    )}
                    //Setting the number of column
                    numColumns={3}
                    keyExtractor={(item, index) => index}
                />
            </View>
        </View>
    );
     
    const SecondRoute = () => (
        <View style={[styles.scene, { backgroundColor: '#ffffff',paddingTop:20 }]}>
            <View>
                <Text style={{margin:10}}>CATEGORY</Text>
                <FlatList
                    data={DATA2}
                    renderItem={({item}) => (
                    <View
                        style={{
                        flex: 1,
                        flexDirection: 'column',
                        margin: 1
                        }}>
                        <TouchableOpacity 
                            style={styles.halfWidth}
                        >
                            <Image 
                            source = {item.image} 
                            style={styles.Category}
                            resizeMode={'contain'} 
                            />
                        </TouchableOpacity>
                    </View>
                    )}
                    //Setting the number of column
                    numColumns={3}
                    keyExtractor={(item, index) => index}
                />
            </View>
        </View>
      );
     
    const SecondRoute3 = () => (
        <View style={[styles.scene, { backgroundColor: '#ffffff',paddingTop:20 }]}>
            <View>
                <Text style={{margin:10}}>CATEGORY</Text>
                <FlatList
                    data={DATA3}
                    renderItem={({item}) => (
                    <View
                        style={{
                        flex: 1,
                        flexDirection: 'column',
                        margin: 1
                        }}>
                        <TouchableOpacity 
                            //onPress={() =>navigation.navigate('SelectItemCatButton', { name: route.params.name })} 
                            style={styles.halfWidth}
                        >
                            <Image 
                            source = {item.image} 
                            style={styles.Category}
                            resizeMode={'contain'} 
                            />
                        </TouchableOpacity>
                    </View>
                    )}
                    //Setting the number of column
                    numColumns={3}
                    keyExtractor={(item, index) => index}
                />
            </View>
        </View>
      );
         
    const initialLayout = { width: Dimensions.get('window').width };
       
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'System' },
    { key: 'second', title: 'Commercial' },
    { key: 'second3', title: 'Air Curtain' },
  ]);
 
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    second3: SecondRoute3,
  });
  return (
    <LinearGradient
            colors={[ '#f7b614','#f7b614', 'white' ]}
            style={styles.fullheight}
            >
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>

            <KeyboardAvoidingView style={styles.containerView} behavior="padding">

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.loginScreenContainer}>
                        <View style={styles.loginFormView}> 
                            <View style={styles.imageWidthHeightView}> 
                                <Image 
                                source = {require("../assets/Logo.png")} 
                                style={styles.QRImageHeight}
                                />
                                <Text style={styles.wareHouseName}>{route.params.name} Warehouse</Text>
                            </View>  
                            <View
                                style={styles.halfWidthButtons}
                                >
                                <TouchableOpacity 
                                    onPress={() =>navigation.navigate('QRScreen', { name: route.params.name })} 
                                >
                                    <Image 
                                    source = {require('../assets/scan-qr.png')} 
                                    style={styles.QRSCANQrButton}
                                    resizeMode={'contain'} 
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    onPress={() =>navigation.navigate('SelectItemCatButton', { name: route.params.name })} 
                                    style={styles.mainButtonsCat}
                                >
                                    <Image 
                                    source = {require('../assets/search.png')} 
                                    style={styles.QRSCANSearchButton}
                                    resizeMode={'contain'} 
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.QRSction}>  
                                <Text style={{margin:15}}>RECENTLY</Text> 
                                <TabView
                                    navigationState={{ index, routes }}
                                    renderScene={renderScene}
                                    onIndexChange={setIndex}
                                    tabBarInactiveTextColor={'black'}
                                    tabBarActiveTextColor={'red'}
                                    initialLayout={initialLayout}
                                    />
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>

        </ScrollView>
    </LinearGradient>
  );
}
 