import React from 'react';
import {
  ScrollView,
  View, 
  TouchableWithoutFeedback, 
  KeyboardAvoidingView, 
  Keyboard,
  Image,
  Text,
  TouchableOpacity, 
  FlatList, 
} from 'react-native';

import styles from "../style/Style";
import LinearGradient from 'react-native-linear-gradient'

const DATA = [
    {
        id: "1",
        image:require('../assets/accessories.png'),
    },
    {
        id: "2",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "3",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "4",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "5",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "6",
        image:require('../assets/accessories.png'),
    },
    {
        id: "7",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "8",
        image:require('../assets/bolt-nut.png'),
    },
    {
        id: "9",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "10",
        image:require('../assets/accessories.png'),
    },
    {
        id: "11",
        image:require('../assets/cable-tray.png'),
    },
    {
        id: "12",
        image:require('../assets/bolt-nut.png'),
    },
  ];
  const RECENTLY = [
    {
        id: "1",
        image:require('../assets/system-button.png'),
    },
    {
        id: "2",
        image:require('../assets/commercial-button.png'),
    },
    {
        id: "3",
        image:require('../assets/ari-curtain-button.png'),
    },
    {
        id: "4",
        image:require('../assets/system-button.png'),
    },
    {
        id: "5",
        image:require('../assets/ari-curtain-button.png'),
    },
  ];

export default function SelectItemCatButton({ navigation, route  }) {
  return (
    <LinearGradient
            colors={[ '#f7b614','#f7b614', 'white' ]}
            style={styles.fullheight}
            >
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>

            <KeyboardAvoidingView style={styles.containerView} behavior="padding">

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.loginScreenContainer}>
                        <View style={styles.loginFormView}> 
                            <View style={styles.imageWidthHeightView}> 
                                <Image 
                                source = {require("../assets/Logo.png")} 
                                style={styles.QRImageHeight}
                                />
                                <Text style={styles.wareHouseName}>{route.params.name} Warehouse</Text>
                            </View>  
                            <View
                                style={styles.halfWidthButtons}
                                >
                                <TouchableOpacity 
                                    onPress={() =>navigation.navigate('QRScreen', { name: route.params.name })} 
                                >
                                    <Image 
                                    source = {require('../assets/scan-qr.png')} 
                                    style={styles.QRSCANQrButton}
                                    resizeMode={'contain'} 
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    onPress={() =>navigation.navigate('SelectItemCatButton', { name: route.params.name })} 
                                    style={styles.mainButtonsCat}
                                >
                                    <Image 
                                    source = {require('../assets/search.png')} 
                                    style={styles.QRSCANSearchButton}
                                    resizeMode={'contain'} 
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{marginLeft:10,marginRight:10}}>
                                <Text style={{marginLeft:5,marginTop:10,marginBottom:10}}>RECENTLY</Text> 
                                <FlatList
                                    horizontal={true} 
                                    data={RECENTLY}
                                    renderItem={({item}) => (
                                    <View
                                        style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        margin: 1
                                        }}>
                                        <TouchableOpacity 
                                        >
                                            <Image 
                                            source = {item.image} 
                                            style={styles.recentlyImage}
                                            resizeMode={'contain'} 
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    )}
                                    keyExtractor={(item, index) => index}
                                />
                            </View>
                            <View style={[styles.scene, { backgroundColor: '#ffffff',paddingTop:10,marginTop:15 }]}>
                                <View>
                                    <Text style={{margin:10}}>CATEGORY</Text>
                                    <FlatList
                                        data={DATA}
                                        renderItem={({item}) => (
                                        <View
                                            style={{
                                            flex: 1,
                                            flexDirection: 'column',
                                            margin: 1
                                            }}>
                                            <TouchableOpacity 
                                                style={styles.halfWidth}
                                                onPress={() =>navigation.navigate('SelectItemByCat', { name: route.params.name })}
                                            >
                                                <Image 
                                                source = {item.image} 
                                                style={styles.Category}
                                                resizeMode={'contain'} 
                                                />
                                            </TouchableOpacity>
                                        </View>
                                        )}
                                        //Setting the number of column
                                        numColumns={3}
                                        keyExtractor={(item, index) => index}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>

        </ScrollView>
    </LinearGradient>
  );
}
 