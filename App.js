/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen  from './ui/LoginScreen';
import ForgotPassword  from './ui/ForgotPassword';
import LandingScreen  from './ui/LandingScreen';
import QRScreen  from './ui/QRScreen';
import SelectItemByCat  from './ui/SelectItemByCat';
import SelectItemCatButton  from './ui/SelectItemCatButton';
import {LogBox,} from 'react-native';

const Stack = createStackNavigator();

LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}>
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ title: 'LoginScreen' }}
        />
        <Stack.Screen 
          name="ForgotPassword" 
          component={ForgotPassword}
          options={{ title: 'ForgotPassword' }} 
        />
        <Stack.Screen 
          name="LandingScreen" 
          component={LandingScreen}
          options={{ title: 'LandingScreen' }} 
        />
        <Stack.Screen 
          name="QRScreen" 
          component={QRScreen}
          options={{ title: 'QRScreen' }} 
        />
        <Stack.Screen 
          name="SelectItemByCat" 
          component={SelectItemByCat}
          options={{ title: 'SelectItemByCat' }} 
        />
        <Stack.Screen 
          name="SelectItemCatButton" 
          component={SelectItemCatButton}
          options={{ title: 'SelectItemCatButton' }} 
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
